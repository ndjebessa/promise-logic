promise-logic
=============

Logic utilities for (complex) promise networks (validity, model finding/checking, equivalence, ...).
